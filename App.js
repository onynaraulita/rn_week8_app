import React, {Component} from 'react';
import {appBookStore, permanentStore} from './src/redux/store';
import App from './src/App';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';

export default class index extends Component {
  render() {
    return (
      <Provider store={appBookStore}>
        <PersistGate persistor={permanentStore} loading={null}>
          <App />
        </PersistGate>
      </Provider>
    );
  }
}
