import IconAkun from './akun.svg';
import IconAkunActive from './akunActive.svg';
import IconHome from './home.svg';
import IconHomeActive from './homeActive.svg';

export {IconAkun, IconAkunActive, IconHome, IconHomeActive};
