import React, {Component} from 'react';
import {Share, TouchableOpacity} from 'react-native';

import {WARNA_ABU_ABU} from '../../utils/constant';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {connect} from 'react-redux';
import {getBookList, getDetailBook} from '../../redux/actions/BookAction';
import {Header} from 'react-native/Libraries/NewAppScreen';
import {
  addFavorites,
  getFavoritesList,
} from '../../redux/actions/FavoritesAction';
import {notifikasi} from '../Notifikasi';

class HeaderIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMovieDetail: {},
      isLoading: false,
      isFav: false,
      currentPage: 0,
      offset: 1,
    };
  }

  onShare = async () => {
    try {
      this.props.getDetailBook(this.props.idDetailBook);
      const result = await Share.share({
        message: `Read ${this.props.dataDetailBook.title} now at BookApp!`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  handleButtonSubmit = async () => {
    const dataBody = {
      bookId: this.props.idDetailBook,
    };

    this.props.addFavorites(dataBody);
    this.props.getFavoritesList();

    notifikasi.configure();
    notifikasi.buatChannel('1');
    notifikasi.kirimNotifikasi(
      '1',
      'Add to favorites Success!',
      `Success add ${this.props.dataDetailBook.title} book to favorite.`,
    );
  };

  render() {
    {
      console.log('~~SEBELUM RETURN~~');
      console.log(typeof this.props.dataFavoritesBook);
      console.log(this.props.dataFavoritesBook);
      console.log(typeof this.props.idDetailBook);
      console.log(this.props.idDetailBook);
    }
    return (
      <>
        <TouchableOpacity
          style={{marginRight: 20}}
          onPress={this.handleButtonSubmit}>
          {
            //validasi datanya
            this.props.dataFavoritesBook &&
            this.props.dataFavoritesBook.length &&
            this.props.dataFavoritesBook.some(
              temp => temp._id === this.props.idDetailBook,
            ) ? (
              <Icon name="heart" size={20} color="red" title="favorite" />
            ) : (
              <Icon
                name="heart"
                size={20}
                color={WARNA_ABU_ABU}
                title="favorite"
              />
            )
          }
        </TouchableOpacity>

        <TouchableOpacity
          style={{marginRight: 20}}
          onPress={() => {
            {
              this.onShare();
            }
          }}>
          <Icon
            name="share-alt"
            size={20}
            color={WARNA_ABU_ABU}
            title="favorite"
          />
        </TouchableOpacity>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    idDetailBook: state.BookReducer.idBook,
    dataFavoritesBook: state.FavoritesReducer.listFavorites,
    dataDetailBook: state.BookReducer.bookDetail,
  };
};
const mapDispatchToProps = {
  addFavorites,
  getFavoritesList,
  getDetailBook,
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderIcon);
