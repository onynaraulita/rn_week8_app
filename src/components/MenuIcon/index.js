import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  WARNA_UTAMA,
  WARNA_DISABLE,
  WARNA_KUNING,
  WARNA_UNGU_GELAP,
} from '../../utils/constant';

const IconHomeActive = () => {
  return <Icon name="home" color={WARNA_KUNING} size={24} />;
};

const IconHomeNonActive = () => {
  return <Icon name="home" color={WARNA_DISABLE} size={24} />;
};

const IconSearchActive = () => {
  return <Icon name="search" color={WARNA_KUNING} size={24} />;
};

const IconSearchNonActive = () => {
  return <Icon name="search" color={WARNA_DISABLE} size={24} />;
};

const IconFavoritesActive = () => {
  return <Icon name="heart" color={WARNA_KUNING} size={24} />;
};

const IconFavoritesNonActive = () => {
  return <Icon name="heart" color={WARNA_DISABLE} size={24} />;
};

export {
  IconHomeActive,
  IconHomeNonActive,
  IconSearchActive,
  IconSearchNonActive,
  IconFavoritesActive,
  IconFavoritesNonActive,
};
