import {
  WARNA_HITAM,
  WARNA_PUTIH,
  WARNA_UNGU_GELAP,
  WARNA_PUTIH_UNGU,
  WARNA_ABUGELAP,
  WARNA_ABU_ABU,
} from '../../utils/constant';
import {
  View,
  Text,
  Stylesheet,
  FlatList,
  StyleSheet,
  Animated,
  ScrollView,
  Dimensions,
} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  pageRound: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 100,
    height: 100,
    position: 'absolute',
    left: windowWidth * 0.38,
    top: windowHeight * 0.7,
    alignContent: 'center',
  },
  container: {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
