import {
  View,
  Text,
  Stylesheet,
  FlatList,
  StyleSheet,
  Animated,
  ScrollView,
  Dimensions,
} from 'react-native';
import {
  WARNA_HITAM,
  WARNA_PUTIH,
  WARNA_UNGU_GELAP,
  WARNA_PUTIH_UNGU,
  WARNA_ABUGELAP,
  WARNA_ABU_ABU,
} from '../../utils/constant';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import slides from './slides';
import Onboardingitem from './Onboardingitem';
import React, {Component, useState, useRef} from 'react';
import {styles} from './style';

const Onboarding = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const slidesref = useRef(null);
  const viewableItemsChanged = useRef(({viewableItems}) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({viewAreaCoveragePercentThreshold: 50}).current;

  return (
    <ScrollView testID="screen-onboarding">
      <View style={styles.container}>
        <View>
          <FlatList
            data={slides}
            renderItem={({item}) => <Onboardingitem item={item} />}
            horizontal
            showsHorizontalScrollIndicator={false}
            pagingEnabled
            bounces={false}
            keyExtractor={item => item.id}
            onScroll={Animated.event(
              [{nativeEvent: {contentOffset: {x: scrollX}}}],
              {
                useNativeDriver: false,
              },
            )}
            scrollEventThrottle={32}
            onViewableItemsChanged={viewableItemsChanged}
            viewabilityConfig={viewConfig}
            ref={slidesref}
          />
        </View>
        <View style={styles.pageRound}>
          {currentIndex == 0 ? (
            <>
              <Icon name="circle" size={12} color={WARNA_UNGU_GELAP} />
              <Icon name="circle" size={12} color={WARNA_ABU_ABU} />
              <Icon name="circle" size={12} color={WARNA_ABU_ABU} />
            </>
          ) : null}

          {currentIndex == 1 ? (
            <>
              <Icon name="circle" size={12} color={WARNA_ABU_ABU} />
              <Icon name="circle" size={12} color={WARNA_UNGU_GELAP} />
              <Icon name="circle" size={12} color={WARNA_ABU_ABU} />
            </>
          ) : null}

          {currentIndex == 2 ? (
            <>
              <Icon name="circle" size={12} color={WARNA_ABU_ABU} />
              <Icon name="circle" size={12} color={WARNA_ABU_ABU} />
              <Icon name="circle" size={12} color={WARNA_UNGU_GELAP} />
            </>
          ) : null}
        </View>
      </View>
    </ScrollView>
  );
};

export default Onboarding;
