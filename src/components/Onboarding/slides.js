import {LogoIcon, LogoIconHorizontalWhite} from '../../assets';

export default [
  {
    id: '1',
    desc: 'Support your reading habit without paying a dime or even taking a trip to the library',
    title: 'Read anywhere and anytime',
    image: require('./../../assets/images/carouselBookApp.png'),
  },
  {
    id: '2',
    desc: 'You can personalize your own bookmark with your favorite book',
    title: 'Bookmark your favorite books',
    image: require('./../../assets/images/carouselFavorites.png'),
  },
  {
    id: '3',
    desc: 'Buy book to read the full version',
    title: 'Buy book easily',
    image: require('./../../assets/images/carouselBuy.png'),
  },
];
