import {
  WARNA_HITAM,
  WARNA_PUTIH,
  WARNA_UNGU_GELAP,
  WARNA_PUTIH_UNGU,
  WARNA_ABUGELAP,
  WARNA_ABU_ABU,
} from '../../utils/constant';
import {
  View,
  Text,
  Stylesheet,
  FlatList,
  StyleSheet,
  Animated,
  ScrollView,
  Dimensions,
} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  container: {
    backgroundColor: WARNA_PUTIH_UNGU,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: windowHeight,
    paddingVertical: 50,
  },
  image: {
    flex: 0.5,
    width: windowWidth * 0.8,
    justifyContent: 'center',
  },
  title: {
    fontSize: 18,
    marginBottom: 10,
    color: WARNA_HITAM,
    textAlign: 'center',
    fontFamily: 'Karla-Bold',
  },
  description: {
    marginBottom: 30,
    color: '#62656b',
    textAlign: 'center',
    paddingHorizontal: 20,
    fontFamily: 'Karla-Medium',
  },
  getStarted: {
    marginTop: 60,
    backgroundColor: WARNA_UNGU_GELAP,
    width: 340,
    color: WARNA_PUTIH_UNGU,
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    paddingVertical: 16,
    paddingHorizontal: 14,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
});
