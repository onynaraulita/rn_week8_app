import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  useWindowDimensions,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {
  WARNA_HITAM,
  WARNA_PUTIH,
  WARNA_UNGU_GELAP,
  WARNA_PUTIH_UNGU,
  WARNA_ABUGELAP,
  WARNA_ABU_ABU,
} from '../../utils/constant';

import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {styles} from './styleOnboarding';

const Onboardingitem = ({item}) => {
  const {width} = useWindowDimensions();
  const navigation = useNavigation();

  return (
    <View style={[styles.container, {width}]}>
      <Image source={item.image} style={styles.image} />

      <View style={{flex: 0.3, alignItems: 'center'}}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.description}>{item.desc}</Text>

        {item.id === '3' ? (
          <View>
            <TouchableOpacity
              onPress={() => {
                navigation.replace('Login');
              }}>
              <Text testID="button-started" style={styles.getStarted}>
                Let's Get Started
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </View>
  );
};

export default Onboardingitem;
