import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {WARNA_UTAMA, WARNA_KUNING, WARNA_DISABLE} from '../../utils/constant';
import {
  IconFavoritesActive,
  IconFavoritesNonActive,
  IconSearchActive,
  IconSearchNonActive,
  IconHomeActive,
  IconHomeNonActive,
} from '../MenuIcon';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const TabItem = ({isFocused, onPress, onLongPress, label}) => {
  const Icon = () => {
    if (label === 'Home')
      return isFocused ? <IconHomeActive /> : <IconHomeNonActive />;
    if (label === 'Search')
      return isFocused ? <IconSearchActive /> : <IconSearchNonActive />;
    if (label === 'Favorites')
      return isFocused ? <IconFavoritesActive /> : <IconFavoritesNonActive />;
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon />
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: isFocused => ({
    fontSize: 12,
    marginTop: 4,
    color: isFocused ? WARNA_KUNING : WARNA_DISABLE,
  }),
});
