import React, {Component} from 'react';
import {Share, TouchableOpacity, Alert} from 'react-native';

import {
  WARNA_ABU_ABU,
  WARNA_ABUGELAP,
  WARNA_UNGU_GELAP,
} from '../../utils/constant';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {connect} from 'react-redux';
import {getBookList, getDetailBook} from '../../redux/actions/BookAction';
import {Header} from 'react-native/Libraries/NewAppScreen';
import {
  addFavorites,
  getFavoritesList,
} from '../../redux/actions/FavoritesAction';
import {notifikasi} from '../Notifikasi';
import {logoutUser} from '../../redux/actions/OtentikasiAction';

class Logout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMovieDetail: {},
      isLoading: false,
      isFav: false,
      currentPage: 0,
      offset: 1,
    };
  }

  onShare = async () => {
    try {
      this.props.getDetailBook(this.props.idDetailBook);
      const result = await Share.share({
        message: `Read ${this.props.dataDetailBook.title} now at BookApp!`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  handleButtonSubmit = async () => {
    const dataBody = {
      bookId: this.props.idDetailBook,
    };

    this.props.addFavorites(dataBody);
    this.props.getFavoritesList();

    notifikasi.configure();
    notifikasi.buatChannel('1');
    notifikasi.kirimNotifikasi(
      '1',
      'Add to favorites Success!',
      `Success add ${this.props.dataDetailBook.title} book to favorite.`,
    );
  };
  handleLogout = () => {
    return Alert.alert(
      'Logout Confirmation',
      'Are you sure you want to log out?',
      [
        // The "Yes" button
        {
          text: 'Yes',
          onPress: () => {
            this.props.logoutUser(dataDariAction => {
              console.log(dataDariAction, 'Tes print data');

              if (dataDariAction.isLogoutSucess === true)
                this.props.navigation.replace('Login');
            });
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: 'No',
        },
      ],
    );
  };
  render() {
    {
      console.log('~~SEBELUM RETURN~~');
      console.log(typeof this.props.dataFavoritesBook);
      console.log(this.props.dataFavoritesBook);
      console.log(typeof this.props.idDetailBook);
      console.log(this.props.idDetailBook);
    }
    return (
      <>
        <TouchableOpacity onPress={this.handleLogout} style={{marginRight: 10}}>
          <Icon
            name="sign-out"
            size={20}
            color={WARNA_UNGU_GELAP}
            title="favorite"
          />
        </TouchableOpacity>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    idDetailBook: state.BookReducer.idBook,
    dataFavoritesBook: state.FavoritesReducer.listFavorites,
    dataDetailBook: state.BookReducer.bookDetail,
  };
};
const mapDispatchToProps = {
  addFavorites,
  getFavoritesList,
  getDetailBook,
  logoutUser: logoutUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);

// const mapStateToProps = state => {
//     return {
//       dataBook: state.BookReducer.listBooks,
//       dataUser: state.OtentikasiReducer.user,
//       dataFavoritesBook: state.FavoritesReducer.listFavorites,
//       dataLoading: state.BookReducer.isLoading,
//     };
//   };
//   const mapDispatchToProps = {
//     getFavoritesList,
//     logoutUser,
//   };

//   export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
