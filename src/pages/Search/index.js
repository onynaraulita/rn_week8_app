import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_ABUGELAP,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
} from '../../utils/constant';

import InternetConnectionAlert from 'react-native-internet-connection-alert';

import {CompInternetConnectionAlert} from '../../components';
import {styles} from './style';
import {connect} from 'react-redux';
import {searchBook} from '../../redux/actions/BookAction';

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listBook: [],
      isLoading: false,
      dataBooks: [],
      refreshing: false,
      searchBook: '',
      newArr: [],
      valueSearch: '',
      onSearch: false,
      inputValue: '',
    };
  }

  // onRefresh = React.useCallback(() => {
  //   setRefreshing(true);
  //   wait(2000).then(() => setRefreshing(false));
  // }, []);

  _onRefresh = async () => {
    console.log('refresh');
  };

  getSearchResult = searchBook => {
    this.setState({onSearch: true});
    console.log('~~~LAGI CARI~~~');
    this.setState({
      newArr: this.props.dataBook
        .filter(temp => temp.original_title.includes(searchBook))
        .map(({id, title, cover_image}) => ({
          id,
          title,
          cover_image,
        })),
    });

    console.log('hasil cari: ' + this.state.newArr);
  };

  componentDidUpdate() {
    if (this.state.inputValue !== this.state.valueSearch) {
      this.props.searchBook(this.state.valueSearch);
      this.setState({inputValue: this.state.valueSearch});
    }
  }

  handleChangeSearch = valueSearch => {
    this.setState({valueSearch});
  };
  render() {
    return (
      <View
        style={{
          paddingVertical: 10,
          paddingHorizontal: 20,
          backgroundColor: WARNA_PUTIH_UNGU,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={true}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
          style={styles.container}>
          {/* {this.state.isLoading ? (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: WARNA_HITAM,
              }}>
              <ActivityIndicator
                size="large"
                color={WARNA_KUNING}
                style={styles.styleLoading}
              />
            </View>
          ) : null} */}

          <Text style={styles.title}>Search Book</Text>
          <View style={{backgroundColor: WARNA_PUTIH_UNGU}}>
            <TextInput
              style={styles.inputBooks}
              defaultValue=""
              placeholder="Book Title"
              placeholderStyle={{fontFamily: 'Karla-Regular'}}
              name="valueSearch"
              onChangeText={this.handleChangeSearch}
            />
            <TouchableOpacity
              onPress={() => {
                this.props.searchBook(this.state.valueSearch);
              }}>
              <Text
                style={{
                  color: WARNA_PUTIH,
                  backgroundColor: WARNA_UNGU_GELAP,
                  alignSelf: 'flex-start',
                  paddingVertical: 10,
                  paddingHorizontal: 20,
                  borderRadius: 4,
                  marginBottom: 10,
                  borderWidth: 0.5,
                  borderBottomColor: WARNA_KUNING,
                  fontFamily: 'Karla-Bold',
                }}>
                Search
              </Text>
            </TouchableOpacity>
          </View>
          {/* {console.log('~~~yg DI SEARCH~~~', this.state.valueSearch)}
          {console.log('~~~PRINT DI SEARCH~~~', this.props.dataSearchBook)} */}

          <View style={styles.latestUpload}>
            {this.props.dataSearchBook.map((item, index) => (
              <View key={index} style={styles.bookItem}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('BookDetail', {
                      idBook: item.id,
                    });
                  }}>
                  <Image
                    style={styles.gambarBook}
                    source={{
                      uri: `${item.cover_image}`,
                    }}
                  />

                  <Text style={styles.judul}>
                    {item.title.length > 20
                      ? `${item.title.substr(0, 20)}...`
                      : item.title}
                  </Text>
                </TouchableOpacity>
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBook: state.BookReducer.listBooks,
    dataUser: state.OtentikasiReducer.user,
    dataDetailBook: state.BookReducer.bookDetail,
    dataSearchBook: state.BookReducer.searchBook,
  };
};
const mapDispatchToProps = {
  searchBook,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
