import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
  WARNA_ABUGELAP,
} from '../../utils/constant'; // export default Register;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 30,
    marginBottom: 40,
  },
  buttonRegister: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 4,
    backgroundColor: WARNA_UNGU_GELAP,
  },
  buttonRegisterDisabled: {
    backgroundColor: 'black',

    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 4,
    backgroundColor: WARNA_ABUGELAP,
  },
  container: {
    width: windowWidth,
    height: windowHeight,
    backgroundColor: WARNA_PUTIH_UNGU,

    paddingHorizontal: windowWidth * 0.07,

    paddingTop: windowHeight * 0.1,
  },

  inputnama: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
  },
  inputemail: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
  },
  inputPassword: {
    flex: 1,
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
  },
  recommendedItem: {
    marginRight: 5,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 18,
    fontFamily: 'Karla-ExtraBold',
    color: WARNA_HITAM,
    marginBottom: 20,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  email: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
});
