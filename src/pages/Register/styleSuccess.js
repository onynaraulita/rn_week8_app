import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
} from '../../utils/constant'; // export default Register;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 30,
    marginBottom: 40,
  },
  buttonPindah: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 4,
    backgroundColor: WARNA_UNGU_GELAP,
  },
  buttonLogin: {
    padding: 10,
    borderRadius: 6,
  },
  container: {
    width: windowWidth,
    height: windowHeight,
    backgroundColor: WARNA_HITAM,
    paddingHorizontal: windowWidth * 0.07,

    paddingTop: windowHeight * 0.1,
  },
});
