import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_UNGU_GELAP,
  WARNA_ORANGE_TUA,
  WARNA_PUTIH_UNGU,
  WARNA_ABUGELAP,
} from '../../utils/constant';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  erorEmail: {color: 'red', marginBottom: 20, fontFamily: 'Karla-Regular'},
  textButton: {
    color: WARNA_PUTIH,
    fontFamily: 'Karla-Bold',
    fontSize: 16,
  },
  buttonLogin: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 4,
    backgroundColor: WARNA_UNGU_GELAP,
  },
  buttonLoginDisabled: {
    backgroundColor: 'black',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderRadius: 4,
    backgroundColor: WARNA_ABUGELAP,
  },
  container: {
    paddingTop: windowHeight * 0.1,
    width: windowWidth,
    height: windowHeight,
    backgroundColor: WARNA_PUTIH_UNGU,
    paddingHorizontal: windowWidth * 0.07,
  },
  inputUsername: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
    fontFamily: 'Karla-Regular',
  },
  inputPassword: {
    flex: 1,
    backgroundColor: WARNA_PUTIH,
    borderRadius: 3,
    padding: 10,
    marginBottom: 20,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: 150,
    height: 30,
    marginBottom: 40,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 18,
    fontFamily: 'Karla-ExtraBold',
    color: WARNA_HITAM,
    marginBottom: 20,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
});
