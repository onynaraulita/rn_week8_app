import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {NavigationContainer} from '@react-navigation/native';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import {CompInternetConnectionAlert} from '../../components';

import {styles} from './style';
import {connect} from 'react-redux';
import {
  getUserToken,
  navigateToHome,
  setUserDataToken,
} from '../../redux/actions/OtentikasiAction';

import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_ABUGELAP,
  WARNA_ORANGE_TUA,
  WARNA_UNGU_GELAP,
  WARNA_UNGU_TERANG,
} from '../../utils/constant';
import {LogoUvi, LogoMaiBook, LogoIconHorizontal} from '../../assets';
import * as Animatable from 'react-native-animatable';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      valueEmail: '',
      valuePassword: '',
      isLoading: false,
      hidePass: true,
      isValidEmail: true,
      isValidPassword: true,
      erorEmail: '',
      erorPass: '',
      validToLogin: false,
    };
  }

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_UNGU_GELAP);
  }

  handleButtonSubmit = () => {
    if (this.state.valueEmail === '' || this.state.valuePassword === '') {
      Alert.alert(
        'Oops, error',
        'Make sure you have input your email and password correctly',
      );
    } else {
      const dataUser = {
        email: this.state.valueEmail,
        password: this.state.valuePassword,
      };

      //callback action
      this.props.getUserToken(dataUser, dataDariAction => {
        console.log(dataDariAction, 'Tes print data');

        if (dataDariAction.isLoginSucess === true)
          this.props.navigation.replace('MainApp');
      });
    }
  };

  handleChangeEmail = valueEmail => {
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const hasilRegex = regex.test(valueEmail);

    if (hasilRegex) {
      this.setState({isValidEmail: true});
      this.setState({valueEmail});
    } else if (!hasilRegex) {
      this.setState({isValidEmail: false});
      this.setState({erorEmail: 'Please enter a valid email address'});
    }
  };

  handleChangePassword = valuePassword => {
    const regex = /^(?=.*\d).{8,}$/;
    const hasilRegex = regex.test(valuePassword);

    if (valuePassword.trim().length < 7) {
      this.setState({valuePassword});
      this.setState({isValidPassword: false});
      this.setState({erorPass: 'Password must min 8 character'});
    } else if (!hasilRegex) {
      console.log('masuk sini');
      this.setState({isValidPassword: false});
      this.setState({erorPass: 'Make sure there is a number and a alphabet'});
    } else if (hasilRegex) {
      this.setState({isValidPassword: true});
      console.log('masuk sana');
    }

    if (valuePassword.trim().length > 7) {
      this.setState({valuePassword});
      this.setState({isValidPassword: true});
    } else {
      this.setState({isValidPassword: false});
    }
  };

  showPassword = () => {
    this.setState({hidePass: !this.state.hidePass});
  };

  handleValidEmail = val => {
    if (this.state.isValidEmail === true) {
      this.setState({isValidEmail: true});
      this.setState({validToLogin: true});
    } else {
      this.setState({isValidEmail: false});
      this.setState({erorEmail: 'Your email is not valid'});
    }
  };

  handleValidPass = val => {
    if (this.state.isValidPassword === true) {
      this.setState({isValidPassword: true});
      this.setState({validToLogin: true});
    } else {
      this.setState({isValidPassword: false});
      this.setState({erorPass: 'Password is not valid'});
    }
  };

  render() {
    return (
      <View style={styles.container} testID="screen-login">
        <ScrollView showsVerticalScrollIndicator={false}>
          <Image source={LogoIconHorizontal} style={styles.logo} />
          <Text style={styles.title}>Please log in to your account</Text>
          <View>
            <View style={styles.form}>
              <TextInput
                testID="input-email"
                style={styles.inputUsername}
                placeholder="Email"
                placeholderStyle={{fontFamily: 'Karla-Regular'}}
                keyboardType="email-address"
                onChangeText={this.handleChangeEmail}
                onEndEditing={e => this.handleValidEmail(e.nativeEvent.text)}
              />
              {this.state.isValidEmail ? null : (
                <Animatable.View animation="fadeInLeft" duration={500}>
                  <Text style={styles.erorEmail}>{this.state.erorEmail}</Text>
                </Animatable.View>
              )}

              <View style={{flexDirection: 'row'}}>
                <TextInput
                  testID="input-password"
                  style={styles.inputPassword}
                  secureTextEntry={this.state.hidePass}
                  placeholder="Password"
                  placeholderStyle={{fontFamily: 'Karla-Regular'}}
                  name="password"
                  onChangeText={this.handleChangePassword}
                  onEndEditing={e => this.handleValidPass(e.nativeEvent.text)}
                />
                <TouchableOpacity
                  onPress={this.showPassword}
                  style={{position: 'absolute', right: 15, top: 15}}>
                  {this.state.hidePass === true ? (
                    <Icon name="eye-slash" size={20} color={WARNA_ABUGELAP} />
                  ) : (
                    <Icon name="eye" size={20} color={WARNA_ABUGELAP} />
                  )}
                </TouchableOpacity>
              </View>
              {this.state.isValidPassword ? null : (
                <Animatable.View animation="fadeInLeft" duration={100}>
                  <Text
                    style={{
                      color: 'red',
                      marginBottom: 20,
                      fontFamily: 'Karla-Regular',
                    }}>
                    {this.state.erorPass}
                  </Text>
                </Animatable.View>
              )}
              <TouchableOpacity
                style={
                  this.state.isValidEmail && this.state.isValidPassword
                    ? styles.buttonLogin
                    : styles.buttonLoginDisabled
                }
                onPress={this.handleButtonSubmit}
                disabled={this.props.dataLoading ? true : false}>
                {this.props.dataLoading ? (
                  <ActivityIndicator size="small" color="white" />
                ) : (
                  <Text testID="button-login" style={styles.textButton}>
                    Log in
                  </Text>
                )}
              </TouchableOpacity>
            </View>
          </View>

          <View style={{marginTop: 40}}>
            <Text
              style={{
                color: WARNA_HITAM,
                textAlign: 'center',
                fontFamily: 'Karla-Regular',
              }}>
              Don't have an account?
            </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Register');
              }}>
              <Text
                testID="button-register"
                style={{
                  fontFamily: 'Karla-ExtraBold',
                  color: WARNA_HITAM,
                  textAlign: 'center',
                }}>
                Sign up
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataLoading: state.OtentikasiReducer.isLoading,
  };
};

const mapDispatchToProps = {
  getUserToken,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
