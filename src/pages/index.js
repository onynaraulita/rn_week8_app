import Home from './Home';
import Splash from './Splash';
import BookDetail from './BookDetail';
import Search from './Search';
import Login from './Login';
import Register from './Register';
import RegisterSuccess from './Register/RegisterSuccess';
import Favorites from './Favorites';

export {Home, Splash, BookDetail, Search, Login, Register, RegisterSuccess, Favorites};
