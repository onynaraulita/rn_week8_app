import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
  WARNA_ABUGELAP,
  WARNA_UNGU_TERANG,
} from '../../utils/constant';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  latestUpload: {
    paddingVertical: 10,
    marginBottom: 100,
  },
  titleBuku: {
    color: WARNA_HITAM,
    width: 0.4 * windowWidth,
    fontFamily: 'Karla-ExtraBold',
    fontSize: 17,
  },
  judul: {
    flexDirection: 'row',
    flex: 1,
  },
  logout: {color: WARNA_PUTIH, flex: 1},
  title: {
    fontSize: 18,
    fontFamily: 'Karla-Bold',
    color: WARNA_UNGU_GELAP,

    flex: 3,
  },
  container: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    backgroundColor: WARNA_PUTIH_UNGU,
    color: WARNA_PUTIH,
  },
  buttonShowMore: {
    backgroundColor: WARNA_UNGU_GELAP,
    alignSelf: 'flex-start',
    color: WARNA_PUTIH_UNGU,
    fontFamily: 'Karla-Medium',
    fontSize: 12,
    paddingVertical: 10,
    paddingHorizontal: 14,
    borderRadius: 2,
  },
  infoBook: {
    marginLeft: windowWidth * 0.04,
    color: WARNA_UNGU_GELAP,
  },
  recommendedItem: {
    marginRight: 5,
  },
  bookItem: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    color: WARNA_UNGU_GELAP,
    borderWidth: 0.7,
    borderRadius: 6,
    borderColor: WARNA_ABUGELAP,
    backgroundColor: WARNA_PUTIH_UNGU,

    shadowColor: 'black',
    shadowOffset: {
      width: 2,
      height: 6,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Karla-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarBook: {
    width: 100,
    height: 140,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
