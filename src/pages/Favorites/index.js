import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
  WARNA_ABUGELAP,
} from '../../utils/constant';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import {ProgressiveImage, CompInternetConnectionAlert} from '../../components';

import {styles} from './style';
import {connect} from 'react-redux';
import {getFavoritesList} from '../../redux/actions/FavoritesAction';
import {logoutUser} from '../../redux/actions/OtentikasiAction';

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

class Favorites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //  isLoading: false,
      isModalVisible: false,
      refreshing: false,
    };
  }

  componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_UNGU_GELAP);

    console.log('~~~DI FAVORITES~~~');
    this.props.getFavoritesList();
  }

  _onRefresh = async () => {
    this.props.getFavoritesList();
  };

  handleLogout = () => {
    return Alert.alert(
      'Logout Confirmation',
      'Are you sure you want to log out?',
      [
        // The "Yes" button
        {
          text: 'Yes',
          onPress: () => {
            this.props.logoutUser(dataDariAction => {
              console.log(dataDariAction, 'Tes print data');

              if (dataDariAction.isLogoutSucess === true)
                this.props.navigation.replace('Login');
            });
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: 'No',
        },
      ],
    );
  };

  render() {
    return (
      <View style={{backgroundColor: WARNA_PUTIH_UNGU}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.props.dataLoading}
              onRefresh={() => this.props.getFavoritesList()}
            />
          }
          style={styles.container}>
          {/* <Text>SEBELUMMM MAP</Text>
          {console.log('~~~LLOADING?~~~', this.props.dataLoading)} */}
          {this.props.dataLoading ? (
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator size="large" color={WARNA_UNGU_GELAP} />
            </View>
          ) : (
            <>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.title}>Favorites</Text>
                <TouchableOpacity onPress={this.handleLogout}>
                  <Icon name="sign-out" size={20} color={WARNA_ABUGELAP} />
                </TouchableOpacity>
              </View>
              {/* {console.log('~~PRINT FAVORITES~~', this.props.dataFavoritesBook)} */}
              {/* 
              var unique = myArray.filter((v, i, a) => a.indexOf(v) === i);  */}
              <View style={styles.latestUpload}>
                {this.props.dataFavoritesBook &&
                this.props.dataFavoritesBook.length ? (
                  this.props.dataFavoritesBook.map((item, index) => (
                    <View key={index} style={styles.bookItem}>
                      <View style={styles.posterLatest}>
                        <Image
                          style={styles.gambarBook}
                          source={{
                            uri: `${item.cover_image}`,
                          }}
                        />
                      </View>
                      <View style={styles.infoBook}>
                        <Text style={styles.titleBuku}>{item.title}</Text>
                        <Text
                          style={{
                            fontFamily: 'Montserrat-Bold',
                            color: WARNA_ABUGELAP,
                            marginTop: 5,
                            marginBottom: 10,
                          }}>
                          {item.author}
                        </Text>

                        <TouchableOpacity
                          onPress={() => {
                            this.props.navigation.navigate('BookDetail', {
                              idBook: item._id,
                            });
                          }}>
                          <Text style={styles.buttonShowMore}>Show More</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ))
                ) : (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1,
                      marginTop: 20,
                    }}>
                    <Icon
                      name="heart"
                      size={80}
                      color={WARNA_ABU_ABU}
                      title="favorite"
                    />
                    <Text style={{marginTop: 20}}>No favorite's yet. </Text>
                  </View>
                )}
              </View>
            </>
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBook: state.BookReducer.listBooks,
    dataUser: state.OtentikasiReducer.user,
    dataFavoritesBook: state.FavoritesReducer.listFavorites,
    dataLoading: state.BookReducer.isLoading,
  };
};
const mapDispatchToProps = {
  getFavoritesList,
  logoutUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
