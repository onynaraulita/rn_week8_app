import {Dimensions, StyleSheet} from 'react-native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_PUTIH_UNGU,
  WARNA_UNGU_GELAP,
  WARNA_ABUGELAP,
} from '../../utils/constant'; // export default Register;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  namaBawah: {
    position: 'absolute',
    left: windowWidth * 0.4,
    bottom: windowHeight * 0.01,
    color: WARNA_KUNING,
    fontFamily: 'Montserrat-Bold',
    fontSize: 10,
  },
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: WARNA_PUTIH_UNGU,
  },
  logo: {
    width: 180,
    height: 100,
  },
});
