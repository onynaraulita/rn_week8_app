import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image, LogBox, Dimensions} from 'react-native';
import {LogoUvi, LogoMaiBook} from '../../assets';
import {
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH_UNGU,
} from '../../utils/constant';

import {styles} from './style';
const Splash = ({navigation}) => {
  //perintah yg akan dijalankan pertama kali
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Onboarding'); //kalo replace, diback akan hilang. kalo navigate, di back akan ke splashscreen
    }, 2000);
  }, [navigation]);

  LogBox.ignoreLogs(['Remote debugger']);
  return (
    <View style={{flex: 1}}>
      <View style={styles.background}>
        <Image
          testID="screen-splash"
          source={LogoMaiBook}
          style={styles.logo}
        />
      </View>
    </View>
  );
};

export default Splash;
