import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native';

import {Image} from 'react-native-elements';
import {
  WARNA_HITAM,
  WARNA_ABUGELAP,
  WARNA_UNGU_GELAP,
} from '../../utils/constant';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {styles} from './style';
import {connect} from 'react-redux';
import {getBookList} from '../../redux/actions/BookAction';
import {getFavoritesList} from '../../redux/actions/FavoritesAction';
import {Loading} from './loading';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      namaUser: '',
    };
  }

  componentDidMount() {
    this.props.getBookList();
    // this.props.getFavoritesList();
  }

  _onRefresh = async () => {
    this.props.getBookList();
  };

  render() {
    // console.log(this.props.dataBook, 'Props book pada halaman home');
    return (
      <ScrollView
        testID="screen-home"
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={this.props.dataLoading}
            onRefresh={this._onRefresh}
          />
        }
        style={styles.container}>
        {this.props.dataLoading ? (
          <ActivityIndicator size="large" color={WARNA_UNGU_GELAP} />
        ) : (
          <View>
            <Text style={{color: WARNA_HITAM, marginBottom: 10}}>
              Halo, {this.props.dataUser.name.split(' ')[0]} !
            </Text>

            <Text style={styles.title}>Recommended</Text>
            <View style={styles.recommended}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {this.props.dataBook
                  .sort((a, b) =>
                    a.average_rating > b.average_rating ? -1 : 1,
                  )
                  .map((item, index) => (
                    <View key={index} style={styles.recommendedItem}>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate('BookDetail', {
                            idBook: item.id,
                          });
                        }}>
                        <View key={index} style={styles.containerBook}>
                          <Image
                            style={styles.gambarBuku}
                            source={{
                              uri: `${item.cover_image}`,
                            }}
                          />
                          <TouchableOpacity
                            onPress={() => {
                              this.props.navigation.navigate('BookDetail', {
                                idBook: item.id,
                              });
                            }}>
                            <Text
                              style={{
                                fontFamily: 'Karla-Bold',
                                color: WARNA_HITAM,
                                width: 95,
                                fontSize: 13,
                                paddingTop: 4,
                              }}>
                              {item.title.length > 20
                                ? `${item.title.substr(0, 20)}...`
                                : item.title}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))}
              </ScrollView>
            </View>

            <Text style={styles.title}>List Books</Text>
            <View
              style={{
                flex: 1,
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-around',
              }}>
              {this.props.dataBook
                .sort((a, b) => (a.id > b.id ? -1 : 1))
                .map((item, index) => (
                  <View
                    key={index}
                    style={{
                      width: 95,
                      marginTop: 24,
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('BookDetail', {
                          idBook: item.id,
                        });
                      }}>
                      <Image
                        style={styles.gambarBuku}
                        source={{
                          uri: `${item.cover_image}`,
                        }}
                      />
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('BookDetail', {
                          idBook: item.id,
                        });
                      }}>
                      <Text
                        style={{
                          fontFamily: 'Karla-Bold',
                          color: WARNA_ABUGELAP,
                          width: 95,
                          fontSize: 13,
                          paddingTop: 8,
                        }}>
                        {item.title.length > 20
                          ? `${item.title.substr(0, 20)}...`
                          : item.title}
                      </Text>
                    </TouchableOpacity>
                    {/* <View style={{flex: 1, flexDirection: 'row', marginTop: 4}}>
                  <Icon name="star" color={WARNA_KUNING} size={16} />
                  <Text
                    style={{
                      fontFamily: 'Montserrat-Regular',
                      fontSize: 12,
                      marginLeft: 6,
                      color: WARNA_PUTIH,
                    }}>
                    {item.average_rating}
                  </Text> 
                </View>
                <Text style={{color: WARNA_PUTIH}}>{item.price}</Text> 
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('BookDetail', {
                      idBook: item.id,
                    });
                  }}>
                  <Text style={styles.buttonShowMore}>Show More</Text>
                </TouchableOpacity> */}
                  </View>
                ))}
            </View>
            {/* </>
        )} */}
          </View>
        )}
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    dataBook: state.BookReducer.listBooks,
    dataUser: state.OtentikasiReducer.user,
    dataLoading: state.BookReducer.isLoading,
  };
};
const mapDispatchToProps = {
  getBookList,
  getFavoritesList,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
