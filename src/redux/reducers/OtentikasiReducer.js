import {
  GET_LOGIN_TOKEN_FAILED,
  GET_LOGIN_TOKEN_SUCCESS,
  GET_LOGIN_TOKEN_START,
  SET_TOKEN_USER,
  SET_DATA_USER,
  LOGOUT_FAILED,
  LOGOUT_SUCCESS,
  LOGOUT_START,
} from '../actions/OtentikasiAction';
import {
  REGISTER_FAILED,
  REGISTER_START,
  REGISTER_SUCCESS,
} from '../actions/RegisterAction';

const initialState = {
  user: {},
  tokenUser: '',
  isLoading: false,
};

export default (state = initialState, action) => {
  console.log(action, 'Page LOGIN - Ini action yang kepanggil');

  switch (action.type) {
    case GET_LOGIN_TOKEN_START:
      return {...state, isLoading: true};
    case GET_LOGIN_TOKEN_FAILED:
      return {...state, isLoading: false};
    case GET_LOGIN_TOKEN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        user: action.dataUser,
        tokenUser: action.tokenUser,
      };
    case REGISTER_START:
      return {...state, isLoading: true};
    case REGISTER_FAILED:
      return {...state, isLoading: false};
    case REGISTER_SUCCESS:
      return {...state, user: action.dataUser, isLoading: false};
    case LOGOUT_START:
      return {...state, isLoading: true};
    case LOGOUT_FAILED:
      return {...state, isLoading: false};
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        tokenUser: action.tokenUser,
      };
    default:
      return state;
  }
};
