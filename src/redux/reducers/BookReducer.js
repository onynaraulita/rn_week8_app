import {
  GET_BOOK_LIST_FAILED,
  GET_BOOK_LIST_SUCCESS,
  GET_BOOK_LIST_START,
  GET_BOOK_DETAIL_SUCCESS,
  GET_BOOK_DETAIL_FAILED,
  GET_BOOK_DETAIL_START,
  SEARCH_BOOK_FAILED,
  SEARCH_BOOK_START,
  SEARCH_BOOK_SUCCESS,
} from '../actions/BookAction';

const initialState = {
  listBooks: [],
  bookDetail: {},
  searchBook: [],
  titlePage: 'Book Page',
  isLoading: false,
  idBook: '',
};

export default (state = initialState, action) => {
  console.log(action, 'Ini action yang kepanggil');

  // console.log('print datalistbook ', action.dataListBook.data.results);
  switch (action.type) {
    case GET_BOOK_LIST_START:
      return {...state, isLoading: true};
    case GET_BOOK_LIST_FAILED:
      return {...state, isLoading: false};
    case GET_BOOK_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        listBooks: action.dataListBook.data.results,
      };
    case GET_BOOK_DETAIL_START:
      return {...state, isLoading: true};
    case GET_BOOK_DETAIL_FAILED:
      return {...state, isLoading: false};
    case GET_BOOK_DETAIL_SUCCESS:
      return {
        ...state,
        bookDetail: action.dataDetailBook.data,
        idBook: action.idDetailBook,
        isLoading: false,
      };
    case SEARCH_BOOK_START:
      return {...state, isLoading: true};
    case SEARCH_BOOK_FAILED:
      return {...state, isLoading: false};
    case SEARCH_BOOK_SUCCESS:
      return {
        ...state,
        searchBook: action.dataSearchBook.data.results,
        isLoading: false,
      };
    default:
      return state;
  }
};
