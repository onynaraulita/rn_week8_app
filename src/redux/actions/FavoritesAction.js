import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Alert} from 'react-native';

export const GET_FAVORITES_LIST_START = '@@BOOK/GET_FAVORITES_LIST_START';
export const GET_FAVORITES_LIST_SUCCESS = '@@BOOK/GET_FAVORITES_LIST_SUCCESS';
export const GET_FAVORITES_LIST_FAILED = '@@BOOK/GET_FAVORITES_LIST_FAILED';
export const ADD_FAVORITES_START = '@@BOOK/ADD_FAVORITES_START';
export const ADD_FAVORITES_SUCCESS = '@@BOOK/ADD_FAVORITES_SUCCESS';
export const ADD_FAVORITES_FAILED = '@@BOOK/ADD_FAVORITES_FAILED';

export const getFavoritesList = () => async dispatch => {
  try {
    console.log('~~~PANGGIL FAVORITELIST~~~');
    dispatch({
      type: GET_FAVORITES_LIST_START,
    });

    const value = await AsyncStorage.getItem('simpanTokenUser');
    const dataBook = await axios.get(
      'http://code.aldipee.com/api/v1/books/my-favorite',
      {
        headers: {Authorization: `Bearer ${value}`},
      },
    );

    console.log('~~~PRINT DATA BOOK~~~', dataBook);

    dispatch({
      type: GET_FAVORITES_LIST_SUCCESS,
      dataFavoritesBook: dataBook.data,
    });
  } catch (error) {
    dispatch({
      type: GET_FAVORITES_LIST_FAILED,
    });
    Alert.alert(error.message);
  }
};

export const addFavorites = idBook => async dispatch => {
  try {
    dispatch({
      type: ADD_FAVORITES_START,
    });

    console.log('~~~ PRINT ID BOOK ~~', idBook);
    const value = await AsyncStorage.getItem('simpanTokenUser');
    console.log('~~ditengah~~', value);

    const fetchFavBook = await axios.post(
      `http://code.aldipee.com/api/v1/books/my-favorite`,
      idBook,
      {
        headers: {Authorization: `Bearer ${value}`},
      },
    );

    // console.log('~~~ PRINT KEYWORD BOOK ~~', keywordBook);
    // const value = await AsyncStorage.getItem('simpanTokenUser');
    // const fetchSearchBook = await axios.get(
    //   `http://code.aldipee.com/api/v1/books?title=${keywordBook}`,
    //   {
    //     headers: {Authorization: `Bearer ${value}`},
    //   },
    // );

    // console.log('~~HASIL CARI~~', fetchSearchBook);

    console.log('setelah');
    console.log(fetchFavBook, 'print');
    if (fetchFavBook.status === 201) {
      dispatch({
        type: ADD_FAVORITES_SUCCESS,
      });
    }
  } catch (error) {
    dispatch({
      type: ADD_FAVORITES_FAILED,
    });
    Alert.alert(error.message);
  }
};
