import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import BookReducer from './reducers/BookReducer';
import OtentikasiReducer from './reducers/OtentikasiReducer';
import FavoritesReducer from './reducers/FavoritesReducer';
import {createLogger} from 'redux-logger';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['BookReducer', 'OtentikasiReducer'],
};

const allReducers = combineReducers({
  BookReducer,
  OtentikasiReducer,
  FavoritesReducer,
});

const permanentReducer = persistReducer(persistConfig, allReducers);

const logger = createLogger({});

export const appBookStore = createStore(
  permanentReducer,
  applyMiddleware(thunk, logger),
);

export const permanentStore = persistStore(appBookStore);
