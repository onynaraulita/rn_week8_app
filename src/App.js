import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import InternetConnectionAlert from 'react-native-internet-connection-alert';
const App = () => {
  return (
    <InternetConnectionAlert
      onChange={connectionState => {
        console.log('Connection State: ', connectionState);
      }}>
      <NavigationContainer>
        <Router />
      </NavigationContainer>
    </InternetConnectionAlert>
  );
};

export default App;
