/* eslint-disable no-undef */
import {device, by, element} from 'detox';

describe('e2e', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  //CHECKED
  test('muncul splash screen', async () => {
    await expect(element(by.id('screen-splash'))).toBeVisible();
  });

  //CHECKED
  test('buka onboarding, swipe, buka  halaman login', async () => {
    await expect(element(by.id('screen-splash'))).toBeVisible();
    await waitFor(element(by.id('screen-onboarding')))
      .toBeVisible()
      .withTimeout(3000);
    await element(by.id('screen-onboarding')).swipe('left');
    await element(by.id('screen-onboarding')).swipe('left');

    await expect(element(by.id('button-started'))).toBeVisible();
    await element(by.id('button-started')).tap({x: 1, y: 6});
    await expect(element(by.id('screen-login'))).toBeVisible();
  });

  //CHECKED
  test('login salah, alert eror, coba login yang benar', async () => {
    await expect(element(by.id('screen-splash'))).toBeVisible();
    await waitFor(element(by.id('screen-onboarding')))
      .toBeVisible()
      .withTimeout(3000);
    await element(by.id('screen-onboarding')).swipe('left');
    await element(by.id('screen-onboarding')).swipe('left');

    await expect(element(by.id('button-started'))).toBeVisible();
    await element(by.id('button-started')).tap({x: 1, y: 6});
    await expect(element(by.id('screen-login'))).toBeVisible();
    await waitFor(element(by.id('screen-login'))).toBeVisible();
    await expect(element(by.id('input-email'))).toBeVisible();
    await expect(element(by.id('input-password'))).toBeVisible();
    await element(by.id('input-email')).replaceText('a@gmail.com');
    await element(by.id('input-password')).replaceText('suka');
    await element(by.id('button-login')).tap();

    //kalo salah, dia tangkap alert eror
    await expect(element(by.text('Email dan password salah!'))).toBeVisible();
    await element(by.text('OK')).tap();

    await element(by.id('input-email')).clearText();
    await element(by.id('input-password')).clearText();

    await element(by.id('input-email')).replaceText('onynaraulita@gmail.com');
    await element(by.id('input-password')).replaceText('telkomathon2');
    await element(by.id('button-login')).tap();

    // await expect(element(by.id('screen-home'))).tobevisible();

    await waitFor(element(by.id('screen-home')))
      .toBeVisible()
      .withTimeout(10000);
  });

  //CHECKED
  test('Testing register dengan akun baru', async () => {
    await expect(element(by.id('screen-splash'))).toBeVisible();
    await waitFor(element(by.id('screen-onboarding')))
      .toBeVisible()
      .withTimeout(3000);
    await element(by.id('screen-onboarding')).swipe('left');
    await element(by.id('screen-onboarding')).swipe('left');

    await expect(element(by.id('button-started'))).toBeVisible();
    await element(by.id('button-started')).tap({x: 1, y: 6});
    await expect(element(by.id('screen-login'))).toBeVisible();
    await element(by.id('button-register')).tap();

    await element(by.id('input-name')).replaceText('AA AA');
    //harus diubah
    await element(by.id('input-email')).replaceText('a1@yopmail.com');
    await element(by.id('input-password')).replaceText('telkomathon2');
    await element(by.id('button-signup')).tap();

    await waitFor(element(by.id('screen-register-success')))
      .toBeVisible()
      .withTimeout(5000);

    await element(by.id('button-login')).tap();
    await expect(element(by.id('screen-login'))).toBeVisible();
  });

  //CHECKED
  test('Testing register dengan akun yang sudah ada', async () => {
    await expect(element(by.id('screen-splash'))).toBeVisible();
    await waitFor(element(by.id('screen-onboarding')))
      .toBeVisible()
      .withTimeout(3000);
    await element(by.id('screen-onboarding')).swipe('left');
    await element(by.id('screen-onboarding')).swipe('left');

    await expect(element(by.id('button-started'))).toBeVisible();
    await element(by.id('button-started')).tap({x: 1, y: 6});
    await expect(element(by.id('screen-login'))).toBeVisible();
    await element(by.id('button-register')).tap();

    await element(by.id('input-name')).replaceText('AA AA');
    await element(by.id('input-email')).replaceText('a@yopmail.com');
    await element(by.id('input-password')).replaceText('telkomathon2');
    await element(by.id('button-signup')).tap();
    await expect(element(by.text('Email has already taken'))).toBeVisible();
    await element(by.text('OK')).tap();
  });
});
